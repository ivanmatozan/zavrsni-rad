$(document).ready(function() {

	// Validate form fields in Sign up form 
	$("form").submit(function(){
		var isFormValid = true;
		$(":text, textarea").each(function(){
			if($(this).val() == '') {
				$(this).addClass("error");
				isFormValid = false;
			} else {
				$(this).removeClass("error");
			}
		});
		
		$("select").each(function(){
			if($(this).val() == '') {
				$(this).addClass("error");
				isFormValid = false;
			} else {
				$(this).removeClass("error");
			}
		});
		
		if (!isFormValid) alert("Please fill in all the required fields (highlighted in red)");
		return isFormValid;
	});

	
	// Add Question Button
	$("#add_question").click(addQuestion);
	
	// Remove Question Button
	$("#remove_question").click(removeQuestion);
	
	// Select Change Survey
	$(document.body).on("change", ".select", formSelect);
	
	// Select Change Poll
	$(document.body).on("change", ".select", formSelect);
	
	// Add Field Button
	$(document.body).on("click", ".add_answer", addField);
	
	// Remove Field Button
	$(document.body).on("click", ".remove_answer", removeField);
		
	// Add Row Button
	$(document.body).on("click", ".add_row", addRow);
	
	// Remove Row Button
	$(document.body).on("click", ".remove_row", removeRow);

	// Add Column Button
	$(document.body).on("click", ".add_column", addColumn);
	
	// Remove Column Button
	$(document.body).on("click", ".remove_column", removeColumn);
	
	
	// Add question Function
	function addQuestion () {
		var i = $('.survey_question').length;
		i++;
		$("<fieldset class='survey_question' id='qid"+ i +"'><legend>Question "+ i +"</legend>"+
		"<p>Question: <input type='text' name='question" + i + "' /></p><br />"+
		"Type: <select id='select"+ i +"' name='select"+ i +"' class='select'><option value=''>Select Type:</option>"+
		"<option value='R'>Radio Buttons</option>"+
		"<option value='RT'>Radio Buttons + Text</option>"+
		"<option value='C'>Checkboxes</option>"+
		"<option value='CT'>Checkboxes + Text</option>"+
		"<option value='AR'>Array (Row)</option>"+
		"<option value='T'>Text</option></select>"+
		"</fieldset>").insertAfter("fieldset:last");
	}
	
	
	// Remove Question Function
	function removeQuestion () {
		var i = $('.survey_question').length;
		if(i > 1) {
			$("fieldset:last").remove();
		}
	}
	
	
	// Select Change Function
	function formSelect () {
		var id = $(this).attr("id").split("select").join(""); // Get id
		var getValue = $("#select" + id).val(); // Get selected value
		var addButton = "<input type='button' value='Add Answer' class='add_answer' id='add_answer"+ id +"' />";
		var removeButton = "<input type='button' value='Remove Answer' class='remove_answer' id='remove_answer"+ id +"' />";
		var addRowBtn = "<input type='button' value='Add Row' class='add_row' id='add_row"+ id +"' />";
		var removeRowBtn = "<input type='button' value='Remove Row' class='remove_row' id='remove_row"+ id +"' />";
		var addColumnBtn = "<input type='button' value='Add Column' class='add_column' id='add_column"+ id +"' />";
		var removeColumnBtn = "<input type='button' value='Remove Column' class='remove_column' id='remove_column"+ id +"' />";
		var arrayOptions = "<select name='array_select["+ id +"][]'><option value=''>Select Type:</option>"+
			"<option value='R'>Radio Buttons</option>"+
			"<option value='C'>Checkboxes</option>"+
			"</select>";
		
		// Buttons
		if (getValue == "R"){
			$("#poll" + id).remove() // Remove previous select
			$("<div id='poll"+ id +"'>").insertAfter("#select" + id);
			$("<br />").appendTo("#poll" + id);
			$(addButton).appendTo("#poll" + id);
			$(removeButton).appendTo("#poll" + id);
			$("<p><input type='text' name='answers["+ id +"][1]' class='field_text"+ id +"' value='Answer 1' onclick='this.select()' /></p>").appendTo("#poll" + id);
			$("<p><input type='text' name='answers["+ id +"][2]' class='field_text"+ id +"' value='Answer 2' onclick='this.select()' /></p>").appendTo("#poll" + id);
			$("</div>").appendTo("#poll" + id);
		}
		
		// Buttons + Text
		else if (getValue == "RT"){
			$("#poll" + id).remove() // Remove previous select
			$("<div id='poll"+ id +"'>").insertAfter("#select" + id);
			$("<br />").appendTo("#poll" + id);
			$(addButton).appendTo("#poll" + id);
			$(removeButton).appendTo("#poll" + id);
			$("<p><input type='text' name='answers["+ id +"][1]' class='field_readonly"+ id +"' value='Readonly Text' readonly></p>").appendTo("#poll" + id);
			$("<p><input type='text' name='answers["+ id +"][2]' class='field_text"+ id +"' value='Answer 1' onclick='this.select()' /></p>").appendTo("#poll" + id);
			$("</div>").appendTo("#poll" + id);
		}
		
		// Checkboxes
		else if (getValue == "C"){
			$("#poll" + id).remove() // Remove previous select
			$("<div id='poll"+ id +"'>").insertAfter("#select" + id);
			$("<br />").appendTo("#poll" + id);
			$(addButton).appendTo("#poll" + id);
			$(removeButton).appendTo("#poll" + id);
			$("<p><input type='text' name='answers["+ id +"][1]' class='field_text"+ id +"' value='Answer 1' onclick='this.select()' /></p>").appendTo("#poll" + id);
			$("<p><input type='text' name='answers["+ id +"][2]' class='field_text"+ id +"' value='Answer 2' onclick='this.select()' /></p>").appendTo("#poll" + id);
			$("</div>").appendTo("#poll" + id);
		}
		
		// Checkboxes + Text
		else if (getValue == "CT"){
			$("#poll" + id).remove() // Remove previous select
			$("<div id='poll"+ id +"'>").insertAfter("#select" + id);
			$("<br />").appendTo("#poll" + id);
			$(addButton).appendTo("#poll" + id);
			$(removeButton).appendTo("#poll" + id);
			$("<p><input type='text' name='answers["+ id +"][1]' class='field_readonly"+ id +"' value='Readonly Text' readonly></p>").appendTo("#poll" + id);
			$("<p><input type='text' name='answers["+ id +"][2]' class='field_text"+ id +"' value='Answer 1' onclick='this.select()' /></p>").appendTo("#poll" + id);
			$("</div>").appendTo("#poll" + id);
		}
		
		// Array (Row)
		else if (getValue == "AR"){
			$("#poll" + id).remove() // Remove previous select
			$("<div id='poll"+ id +"'>").insertAfter("#select" + id);
			$("<br />").appendTo("#poll" + id);
			$(addColumnBtn).appendTo("#poll" + id);
			$(removeColumnBtn).appendTo("#poll" + id);
			$("<table id='array_table"+ id +"'>").appendTo("#poll" + id);
			$("<tr class='a_row"+ id +"' id='a_row1'><td></td><td><input type='text' class='array_answers' id='array_answers"+ id +"' name='array_answers["+ id +"][]' value='Answer1' onclick='this.select()' /></td><td><input type='text' class='array_answers' id='array_answers"+ id +"' name='array_answers["+ id +"][]' value='Answer2' onclick='this.select()' /></td></tr>").appendTo("#array_table" + id);
			$("<tr class='row_row' ><td><input type='text' name='array_questions["+ id +"][]' value='Question' onclick='this.select()' /></td><td>"+ arrayOptions +"</td><td></td></tr>").appendTo("#array_table" + id);
			$(addRowBtn).appendTo("#poll" + id);
			$(removeRowBtn).appendTo("#poll" + id);
			$("</table>").appendTo("#poll" + id);
			$("</div>").appendTo("#poll" + id);
		}
		
		// Text
		else if (getValue == "T"){
			$("#poll" + id).remove() // Remove previous select
			$("<div id='poll"+ id +"'>").insertAfter("#select" + id);
			$("<br />").appendTo("#poll" + id);
			$("<textarea type='text' name='survey_text["+ id +"]' rows='4' readonly>Readonly Textarea</textarea>").appendTo("#poll" + id);
			$("</div>").appendTo("#poll" + id);
		}
	}

	
	// Add Field Function
	function addField () {
		var id = $(this).attr("id").split("add_answer").join(""); // Get id
		var i = $(".field_text" + id).length;
		i++;
		$("<p><input type='text' name='answers["+ id +"]["+ i +"]' id='aid"+ i +"' class='field_text"+ id +"' value='Answer "+ i +"' onclick='this.select()' /></p>").appendTo("#poll" + id);
	}
	
	// Remove Field Function
	function removeField () {
		var id = $(this).attr("id").split("remove_answer").join(""); // Get id
		var y = $('.field_text' + id).length;
		var z = $('.field_readonly' + id).length;
		var x = y + z;
		if(x >= 3) {
				$(".field_text"+ id +":last").remove();
		}
	}
	

	// Add Row Function
	function addRow () {
		var id = $(this).attr("id").split("add_row").join(""); // Get id
		$("#array_table"+ id +" tbody>tr:last").clone(true).insertAfter("#array_table"+ id +" tbody>tr:last");
	}
	
	// Remove Row Function
	function removeRow () {
		var id = $(this).attr("id").split("remove_row").join(""); // Get id
		var i = $("#array_table"+ id +" tr.row_row").length; // Count number of question rows
		if (i > 1) {
			$("#array_table"+ id +" tbody>tr:last").remove();
		}
	}
	
	// Add Column Function
	function addColumn () {
		var id = $(this).attr("id").split("add_column").join(""); // Get id
		var arrayOptions = "<select name='array_select["+ id +"][]'><option value=''>Select Type:</option>"+
			"<option value='R'>Radio Buttons</option>"+
			"<option value='C'>Checkboxes</option>"+
			"</select>";
		var r = $(".a_row"+ id).length; // Count number answers rows
		var i = $(".array_answers#array_answers"+ id).length; // Count number cells
		if (i%6 === 0) {
			r++;
			var p = r-1;
			$("<tr class='a_row"+ id +"' id='a_row"+ r +"'><td></td>").insertAfter(".a_row"+ id +"#a_row"+ p);
		}
		i++;
		$("<td><input type='text' class='array_answers' id='array_answers"+ id +"' name='array_answers["+ id +"][]' value='Answer"+ i +"'  onclick='this.select()'/></td>").appendTo(".a_row"+ id +"#a_row" + r);
		$("<td></td>").appendTo("#array_table"+ id +" .row_row");
	}
	
	// Remove Column Function
	function removeColumn () {
		var id = $(this).attr("id").split("remove_column").join(""); // Get id
		var i = $(".array_answers#array_answers"+ id).length; // Count number of cells
		var r = $(".a_row"+ id).length; // Count number answers rows
			if (i > 2) {
				$(".a_row"+ id +"#a_row"+ r +" td:last-child").remove();
				var z = $(".a_row"+ id +"#a_row"+ r +" td").length; // Count number of cells in row
				if (z < 2) {
					$(".a_row"+ id +"#a_row"+ r).remove()
				}
			}
	}

});