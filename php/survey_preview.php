﻿<?php

include "connect.php";

mysqli_set_charset($con, "utf8");

$sid = $_GET['id'];

$survey = mysqli_query($con, "SELECT * FROM surveys WHERE sid='$sid'") or die (mysqli_error($con));

$s_row = mysqli_fetch_array($survey);

echo "Name: ".$s_row['name']."<br />";
echo "Author: ".$s_row['author']."<br />";
echo "Description: ".$s_row['description']."<br />";
echo "Created: ".$s_row['date_created']."<br />";
echo "<br />";

$questions = mysqli_query($con, "SELECT * FROM survey_questions WHERE sid='$sid' ORDER BY qid") or die (mysqli_error($con));

while($q_row = mysqli_fetch_array($questions)) {
	
	$qid = $q_row['qid'];
	 
	echo "<fieldset><legend>Question ".$qid."</legend>";
	echo "Question: ".$q_row['question']."<br />";
	echo "<br />";
	
	$answers = mysqli_query($con, "SELECT * FROM survey_answers WHERE sid='$sid' AND qid='$qid'  ORDER BY aid") or die (mysqli_error($con));
	

	if ($q_row['type'] == "R") {
		echo "<form>";
		while($a_row = mysqli_fetch_array($answers)) {
			echo "<input type='radio' name='question".$qid."' value='".$a_row['answer']."'>".$a_row['answer']."<br />";
		}
		echo "</form>";
	}
	
	else if ($q_row['type'] == "RT") {
		echo "<form>";
		while($a_row = mysqli_fetch_array($answers)) {
			if ($a_row['answer'] == "Readonly Text") {
				echo "<input type='radio' name='question".$qid."'><input type='text'><br />";
			}
			else {
				echo "<input type='radio' name='question".$qid."' value='".$a_row['answer']."'>".$a_row['answer']."<br />";
			}
		}
		echo "</form>";
	}
	
	else if ($q_row['type'] == "C") {
		echo "<form>";
		while($a_row = mysqli_fetch_array($answers)) {
			echo "<input type='checkbox' name='question".$qid."' value='".$a_row['answer']."'>".$a_row['answer']."<br />";
		}
		echo "</form>";
	}
	
	else if ($q_row['type'] == "CT") {
		echo "<form>";
		while($a_row = mysqli_fetch_array($answers)) {
			if ($a_row['answer'] == "Readonly Text") {
				echo "<input type='checkbox' name='question".$qid."'><input type='text'><br />";
			}
			else {
				echo "<input type='checkbox' name='question".$qid."' value='".$a_row['answer']."'>".$a_row['answer']."<br />";
			}
		}
		echo "</form>";
	}
	
	else if ($q_row['type'] == "AR") {
		$q_answers = mysqli_query($con, "SELECT * FROM survey_array_questions WHERE sid='$sid' AND qid='$qid'  ORDER BY aqid") or die (mysqli_error($con));
		$a_answers = mysqli_query($con, "SELECT * FROM survey_array_answers WHERE sid='$sid' AND qid='$qid'  ORDER BY aaid") or die (mysqli_error($con));
		echo "<table><tr><td></td>";
		$c = 0;
		while($aa_row = mysqli_fetch_array($a_answers)) {
			echo "<td align='center'>".$aa_row['answer']."</td>";
			$c++;
		}
		echo "</tr>";
		while($aq_row = mysqli_fetch_array($q_answers)) {
			$aqid = $aq_row['aqid'];
			if ($aq_row['type'] == "R") {
				echo "<tr><td>".$aq_row['question']."</td>";
				for ($i=1; $i<=$c; $i++) {
					echo "<td><input type='radio' name='q".$aqid."'></td>";
				}
				echo "</tr>";
			}
			else if ($aq_row['type'] == "C") {
				echo "<tr><td>".$aq_row['question']."</td>";
				for ($i=1; $i<=$c; $i++) {
					echo "<td><input type='checkbox' name='q".$aqid."'></td>";
				}
				echo "</tr>";
			}
		}
		echo"</table>";
	}
	
	else if ($q_row['type'] == "T") {
		echo "<textarea type='text' name='question".$qid."' rows='4'></textarea>";
	}
	
	echo "</fieldset>";
}

?>