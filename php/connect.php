<?php
// Authentication to database
$dbhost = 'localhost'; // Either a host name or an IP address
$dbuser = 'root'; // The MySQL user name
$dbpass = 'root'; // The password to log in with
$dbname = 'zavrsni'; // The default database to be used when performing queries

// Create connection
$con=mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
// Check connection
if (mysqli_connect_errno($con))
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
?>