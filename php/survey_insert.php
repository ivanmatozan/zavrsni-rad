﻿<?php

include "connect.php";

mysqli_set_charset($con, "utf8");

mysqli_query($con, "INSERT INTO surveys (name, author, description) VALUES ('$_POST[name]', '$_POST[author]', '$_POST[description]')");

$sid = mysqli_insert_id($con);

if (isset($_POST['answers'])) {
	$answers = $_POST['answers'];
	foreach ($answers as $qid => $val) {
		$question = "question".$qid;
		$select = "select".$qid;
		mysqli_query($con, "INSERT INTO survey_questions (sid, qid, question, type) VALUES ('$sid', '$qid', '$_POST[$question]', '$_POST[$select]')");
		foreach ($val as $aid => $value) {
			mysqli_query($con, "INSERT INTO survey_answers (sid, qid, aid, answer) VALUES ('$sid', '$qid', '$aid', '$value')");
		}
	}
}

if (isset($_POST['array_questions'])) {
	$array_questions = $_POST['array_questions'];
	foreach ($array_questions as $qid => $val) {
		$question = "question".$qid;
		$select = "select".$qid;
		mysqli_query($con, "INSERT INTO survey_questions (sid, qid, question, type) VALUES ('$sid', '$qid', '$_POST[$question]', '$_POST[$select]')");
		foreach ($val as $temp_aqid => $value) {
			$type = $_POST['array_select'][$qid][$temp_aqid];
			$aqid = $temp_aqid + 1;
			mysqli_query($con, "INSERT INTO survey_array_questions (sid, qid, aqid, question, type) VALUES ('$sid', '$qid', '$aqid', '$value', '$type')");
		}
	}
}

if (isset($_POST['array_answers'])) {
	$array_answers = $_POST['array_answers'];
	foreach ($array_answers as $qid => $val) {
		foreach ($val as $aaid => $value) {
			$aaid ++;
			mysqli_query($con, "INSERT INTO survey_array_answers (sid, qid, aaid, answer) VALUES ('$sid', '$qid', '$aaid', '$value')");
		}
	}
}

if (isset($_POST['survey_text'])) {
	$text = $_POST['survey_text'];
	foreach ($text as $qid => $value) {
		$question = "question".$qid;
		$select = "select".$qid;
		mysqli_query($con, "INSERT INTO survey_questions (sid, qid, question, type) VALUES ('$sid', '$qid', '$_POST[$question]', '$_POST[$select]')");
	}
}

header("Location: ../index.php?page=surveys");

?>