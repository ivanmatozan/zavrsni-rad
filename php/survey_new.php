<?php
echo "<div class='mainbody'>";

echo "<form action='php/survey_insert.php' method='post'>";

echo "<fieldset id='survey_field'><legend>Survey</legend><table>";
echo "<tr><td>Survey Name:</td><td><input type='text' name='name' /></td></tr>
	<tr><td>Author:</td><td><input type='text' name='author' /></td></tr>
	<tr><td>Description:</td>
	<td><textarea type='text' name='description' rows='4'></textarea></td>
	</tr>";
echo "</table></fieldset>";

echo "<fieldset class='survey_question' id='qid1'><legend>Question 1</legend>";
echo "<p>Question: <input type='text' name='question1' /></p><br />
	Type: <select id='select1' name='select1' class='select'>
		<option value=''>Select Type:</option>
		<option value='R'>Radio Buttons</option>
		<option value='RT'>Radio Buttons + Text</option>
		<option value='C'>Checkboxes</option>
		<option value='CT'>Checkboxes + Text</option>
		<option value='AR'>Array (Row)</option>
		<option value='T'>Text</option>
	</select>";
echo "</fieldset>";

echo "<input type='button' value='Add Question' id='add_question' />
	<input type='button' value='Remove Question' id='remove_question' />
	<input type='submit' value='Save Survey' />";
echo "</form></div>";
?>