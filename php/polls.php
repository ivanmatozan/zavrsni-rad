<?php

echo "<div class='mainbody'>";

include "connect.php";

mysqli_set_charset($con, "utf8");

$result = mysqli_query($con, "SELECT * FROM polls ORDER BY date_created");

$i = 0;

while($row = mysqli_fetch_array($result)) {
	$i++;
	echo "<fieldset><legend>Poll ".$i."</legend>";
	echo "Question: ".$row['question']."<br />";
	echo "Author: ".$row['author']."<br />";
	echo "Created: ".$row['date_created']."<br />";
	echo "<div class='delete'><a href='php/poll_delete.php?id=".$row['pid']."'>Delete</a></div>";
	echo "<div class='preview'><a href='php/poll_preview.php?id=".$row['pid']."' target='_blank'>Preview</a></div>";
	echo "</fieldset>";
}

echo "</div>";

?>