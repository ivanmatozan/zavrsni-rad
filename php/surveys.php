<?php

echo "<div class='mainbody'>";

include "connect.php";

mysqli_set_charset($con, "utf8");

$result = mysqli_query($con, "SELECT * FROM surveys ORDER BY date_created");

$i = 0;

while($row = mysqli_fetch_array($result)) {
	$i++;
	echo "<fieldset><legend>Survey ".$i."</legend>";
	echo "Name: ".$row['name']."<br />";
	echo "Author: ".$row['author']."<br />";
	echo "Description: ".$row['description']."<br />";
	echo "Created: ".$row['date_created']."<br />";
	echo "<div class='delete'><a href='php/survey_delete.php?id=".$row['sid']."'>Delete</a></div>";
	echo "<div class='preview'><a href='php/survey_preview.php?id=".$row['sid']."' target='_blank'>Preview</a></div>";
	echo "</fieldset>";
}


echo "</div>";


?>