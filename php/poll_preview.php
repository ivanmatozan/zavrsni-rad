﻿<?php

include "connect.php";

mysqli_set_charset($con, "utf8");

$pid = $_GET['id'];

$poll = mysqli_query($con, "SELECT * FROM polls WHERE pid='$pid'");

$p_row = mysqli_fetch_array($poll);

echo "<fieldset><legend>Poll ".$pid."</legend>";
echo "Question: ".$p_row['question']."<br />";
echo "Author: ".$p_row['author']."<br />";
echo "Created: ".$p_row['date_created']."<br />";
echo "<br />";

$answers = mysqli_query($con, "SELECT * FROM poll_answers WHERE pid='$pid' ORDER BY aid") or die (mysqli_error($con));

	if ($p_row['type'] == "R") {
		echo "<form>";
		while($a_row = mysqli_fetch_array($answers)) {
			echo "<input type='radio' name='question".$pid."' value='".$a_row['answer']."'>".$a_row['answer']."<br />";
		}
		echo "</form>";
	}
	
	else if ($p_row['type'] == "RT") {
		echo "<form>";
		while($a_row = mysqli_fetch_array($answers)) {
			if ($a_row['answer'] == "Readonly Text") {
				echo "<input type='radio' name='question".$pid."'><input type='text'><br />";
			}
			else {
				echo "<input type='radio' name='question".$pid."' value='".$a_row['answer']."'>".$a_row['answer']."<br />";
			}
		}
		echo "</form>";
	}
	
	else if ($p_row['type'] == "C") {
		echo "<form>";
		while($a_row = mysqli_fetch_array($answers)) {
			echo "<input type='checkbox' name='question".$pid."' value='".$a_row['answer']."'>".$a_row['answer']."<br />";
		}
		echo "</form>";
	}
	
	else if ($p_row['type'] == "CT") {
		echo "<form>";
		while($a_row = mysqli_fetch_array($answers)) {
			if ($a_row['answer'] == "Readonly Text") {
				echo "<input type='checkbox' name='question".$pid."'><input type='text'><br />";
			}
			else {
				echo "<input type='checkbox' name='question".$pid."' value='".$a_row['answer']."'>".$a_row['answer']."<br />";
			}
		}
		echo "</form>";
	}

echo "</fieldset>";

?>