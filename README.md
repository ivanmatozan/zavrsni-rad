# zavrsni-rad
Aplikacija za kreiranje anketa (eng. Poll, Survey)

Cilj završnog rada je osmisliti i napraviti aplikaciju koja će služiti za kreiranje anketa (eng. Poll)
koje će imati mogućnost postavljanja više vrsta pitanja i odgovora. Aplikacija također mora
imati mogućnost kreiranja istraživanja (eng. Survey). Istraživanje je skup od dvije ili više anketa
koje služe za opsežnije prikupljanje podataka. Nakon kreiranja anketa ili istraživanja potrebno ih
je spremiti za kasnije korištenje.

Aplikacije je osmišljena kao web stranica te su za njenu izradu korišteni jezici za web
programiranje kao što su: HTML, CSS, JavaScript, PHP i MySQL. Za jednostavniji rad sa
JavaScript jezikom korištena je njegova najpopularnija biblioteka jQuery. Pošto je aplikacija
web stranica pomoću HTML i CSS jezika napravljen je statični dio ili kostur aplikacije u kojemu
će se izvršavati skripte za dinamično dodavanje sučelja potrebnog za izradu anketa. PHP i
jQuery skriptama stvaramo sučelje pomoću kojega kreiramo ankete ili istraživanja te ih
spremamo u MySQL bazu podataka.
